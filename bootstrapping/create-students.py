import pymongo
import csv
import sys, getopt, random, os

from pymongo import MongoClient
import csv_splitter


def main(argv):
	# Assume non-distributed mode
	distributed_mode = False

	# number of students to create
	n = 3000

	# institution
	institution_ids = []

	# grade levels
	grade_levels = []

	# default connection string
	connection = "mongodb://mongo_admin:password123@localhost:27017/art"

	# SSH key path for distributed mode
	ssh_key_path = "~/.ssh/tds/ssh-dev.pem"

	try:
		opts, args = getopt.getopt(argv, "hn:i:g:c:d:", ["help", "number=", "institutions=", "grades=", "connection=", "distributed="])
	except getopt.GetoptError:
		usage()
		sys.exit(2)

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage();
			sys.exit()
		elif opt in ("-n", "--number"):
			print "Number of students = " + arg
			n = int(arg)
		elif opt in ("-i", "--institutions"):
			print "Institutions = " + arg
			institution_ids = arg.split(',')
		elif opt in ("-c", "--connection"):
			print "Connection string = " + arg
			connection = arg
		elif opt in ("-g", "--grades"):
			print "Grades = " + arg
			grade_levels = arg.split(',')
		elif opt in ("-d", "--distributed"):
			print "Distributed Mode: true - SSH Key path: " + arg
			distributed_mode = True
			ssh_key_path = arg

	# default to grades 3-12 if none provided
	if (len(grade_levels) == 0):
		grade_levels = range(3, 12)

	client = MongoClient(connection)
	db = client.art

	# clean up grade levels to make sure that they are 2 digits
	for idx, grade in enumerate(grade_levels):
		grade_levels[idx] = "{0:0>2}".format(grade_levels[idx])

	# get the matching institutions objects
	if (len(institution_ids) != 0):
		institutions = list(db.institutionEntity.find({ "entityId" : { "$in" : institution_ids } }))
	else:
		institutions = list(db.institutionEntity.find({}))
	
	total_grade_levels = len(grade_levels)
	total_institutions = len(institutions)

	print "Creating student_logins.csv at current directory."
	# open students file for reading
	with open ('student_logins.csv', 'w') as csvfile:
		fieldnames = ['StateAbbreviation', 'ResponsibleDistrictIdentifier', 'ResponsibleInstitutionIdentifier', 'LastOrSurname', 'FirstName', 'MiddleName',
		'Birthdate', 'SSID', 'ExternalSSID', 'GradeLevelWhenAssessed', 'Sex', 'HispanicOrLatinoEthnicity', 'AmericanIndianOrAlaskaNative', 'Asian', 
		'BlackOrAfricanAmerican', 'White', 'NativeHawaiianOrOtherPacificIslander', 'DemographicRaceTwoOrMoreRaces', 'IDEAIndicator', 'LEPStatus', 
		'Section504Status', 'EconomicDisadvantageStatus', 'LanguageCode', 'EnglishLanguageProficiencyLevel', 'MigrantStatus', 'FirstEntryDateIntoUSSchool',
		'LimitedEnglishProficiencyEntryDate', 'LEPExitDate', 'TitleIIILanguageInstructionProgramType', 'PrimaryDisabilityType', 'Delete']
		
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		# SSID, Alt SSID must be unique
		for i in range(0, n):
			studentNum = str(i)
			institutionObject = institutions[random.randint(0, total_institutions-1)]
			grade_level = grade_levels[random.randint(0, total_grade_levels-1)]

			# the 6 race options are all set to NO to start, then we randomly make 1 YES
			race = ["NO","NO","NO","NO","NO","NO"]
			race[random.randint(0,5)] = "YES"

			writer.writerow({
				'StateAbbreviation': institutionObject['stateAbbreviation'], 
				'ResponsibleDistrictIdentifier': institutionObject['parentEntityId'], 
				'ResponsibleInstitutionIdentifier': institutionObject['entityId'],
				'LastOrSurname': 'LastName' + studentNum, 
				'FirstName': 'Name' + studentNum, 
				'MiddleName': 'MiddleName' + studentNum,
				'Birthdate': '', 
				'SSID': 'STUDENTTEST' + studentNum, 
				'AlternateSSID': 'ASTUDENTTEST' + studentNum, 
				'GradeLevelWhenAssessed': grade_level, 
				'Sex': "Female" if random.randint(0, 1) == 0 else "Male", # 50/50 male/female
				'HispanicOrLatinoEthnicity': race[0],
				'AmericanIndianOrAlaskaNative': race[1], 
				'Asian': race[2], 
				'BlackOrAfricanAmerican': race[3], 
				'White': race[4], 
				'NativeHawaiianOrOtherPacificIslander': race[5],
				'DemographicRaceTwoOrMoreRaces': 'NO', 
				'IDEAIndicator': 'NO', 
				'LEPStatus': 'NO', 
				'Section504Status': 'NO', 
				'EconomicDisadvantageStatus': 'NO',
				'LanguageCode': '', 
				'EnglishLanguageProficiencyLevel': '', 
				'MigrantStatus': 'NO', 
				'FirstEntryDateIntoUSSchool': '', 
				'LimitedEnglishProficiencyEntryDate': '',
				'LEPExitDate': '', 
				'TitleIIILanguageInstructionProgramType': '', 
				'PrimaryDisabilityType': '',
				'Delete': ''
			})
	print "Created proctor seed file with " + str(n) + " students."

	if distributed_mode:
		# Get path of python file (where the jmeter_servers file should be located)
		__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

		# Get number of nodes (lines in jmeter_servers file) that we need to break csv file into
		num_nodes = sum(1 for line in open(os.path.join(__location__, 'jmeter_servers')) if line.rstrip())
		print "Number of nodes detected in jmeter_servers file: " + str(num_nodes)

		# Split students into student_logins_<n>.csv so that each file can be SCP'd to host systems.
		csv_splitter.split(open('student_logins.csv', 'r'), ',', n / num_nodes, 'student_logins_%s.csv');

		# Iterate over each server hostname and scp the seed file to the node
		with open(os.path.join(__location__, 'jmeter_servers')) as jmeter_servers:
			for i, line in enumerate(jmeter_servers):
				host = str(line).strip()
				# Get partition seed file path
				student_logins_part_path = "./student_logins_" + str(i + 1) + ".csv"
				print "Executing SCP transfer of student_logins_" + str(i + 1) + ".csv to host " + host + "..."
				# Do an scp (disable host check since we assume these host IPs are secure)
				os.system("scp -o StrictHostKeyChecking=no -i " + ssh_key_path + " " + student_logins_part_path +
						  " ubuntu@" + host + ":~/load-test/student_logins.csv")
				print "Transfer to host " + host + " has completed. Cleaning up " + student_logins_part_path + "..."
				os.remove(student_logins_part_path)
				print "Cleanup complete!"


def usage():
	print "Help/usage details:"
	print "  -c, --connection 	: mongo connection string (defaults to mongodb://localhost:27017/)"
	print "  -n, --number     	: the number of students to create"
	print "  -d, --distributed	: distributed mode - the \"jmeter_nodes\" file will be read to indicate how many pieces" \
		  " the seed file should be broken into and will be sent to the host addresses in this file. Should be followed by path of ssh key"
	print "  --institutions   	: comma separated list of institution entityIds to use (from the institutionEntity collection). if none provided it will use all available"
	print "  --grades         	: comma separated list of grade levels to choose from. if none provided it will use 3-12"
	print "  -h, --help       	: this help screen"



main(sys.argv[1:])
