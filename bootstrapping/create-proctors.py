
import csv
import sys, getopt, os
import csv_splitter


def main(argv):

	# DEFAULTS #
	# number of proctors to create
	n = 100
	usernamePrefix = "proctor"
	domainSuffix = "example.com"
	password = "password123"
	distributed_mode = False
	ssh_key_path = "~/.ssh/tds/ssh-dev.pem"

	try:
		opts, args = getopt.getopt(argv, "hn:u:d:p:d:", ["help", "number=", "user=", "domain=", "password=", "distributed="])
	except getopt.GetoptError:
		usage()
		sys.exit(2)

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage();
			sys.exit()
		elif opt in ("-n", "--number"):
			print "Number of proctors = " + arg
			n = int(arg)
		elif opt in ("-u", "--user"):
			usernamePrefix = arg
			print "Proctor username prefix = " + arg
		elif opt in ("-d", "--domain"):				
			domainSuffix = arg
			print "Proctor domain suffix = " + arg
		elif opt in ("-p", "--password"):
			password = arg
			print "Proctor password = " + arg
		elif opt in ("-d", "--distributed"):
			distributed_mode = True
			ssh_key_path = arg

	print "Creating proctor_logins.csv at current directory."
	# open proctors file for reading
	with open ('proctor_logins.csv', 'w') as csvfile:
		fieldnames = ['proctor_email', 'proctor_password']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()

		for i in range(0, n):
			proctorNum = str(i)
			proctorEmail = usernamePrefix + proctorNum + "@" + domainSuffix
			writer.writerow({
				'proctor_email': proctorEmail, 
			    'proctor_password': password
			    })
	print "Created proctor seed file with " + str(n) + " proctors."

	if distributed_mode:
		# Get path of python file (where the jmeter_servers file should be located)
		__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

		# Get number of nodes (lines in jmeter_servers file) that we need to break csv file into
		num_nodes = sum(1 for line in open(os.path.join(__location__, 'jmeter_servers')) if line.rstrip())
		print "Number of nodes detected in jmeter_servers file: " + str(num_nodes)

		# Split students into student_logins_<n>.csv so that each file can be SCP'd to host systems.
		csv_splitter.split(open('proctor_logins.csv', 'r'), ',', n / num_nodes, 'proctor_logins_%s.csv');

		# Iterate over each server hostname and scp the seed file to the node
		with open(os.path.join(__location__, 'jmeter_servers')) as jmeter_servers:
			for i, line in enumerate(jmeter_servers):
				host = str(line).strip()
				# Get partition seed file path
				proctor_logins_part_path = "./proctor_logins_" + str(i + 1) + ".csv"

				print "Executing SCP transfer of proctor_logins_" + str(i + 1) + ".csv to host " + host + "..."
				# Do an scp (disable host check since we assume these host IPs are secure)
				os.system("scp -o StrictHostKeyChecking=no -i " + ssh_key_path + " " + proctor_logins_part_path +
						  " ubuntu@" + host + ":~/load-test/proctor_logins.csv")

				print "Transfer to host " + host + " has completed. Cleaning up " + proctor_logins_part_path + "..."
				os.remove(proctor_logins_part_path)
				print "Cleanup complete!"


def usage():
	print "Help/usage details:"
	print "  -n,  --number    	: the number of proctors to create"
	print "  -u,  --user      	: the username prefix of all proctor users"
	print "  -d   --domain    	: the email domain suffix"
	print "  -p,  --password  	: the password for all proctor users"
	print "  -h,  --help      	: this help screen"
	print "  -d,  --distributed	: distributed mode - the \"jmeter_nodes\" file will be read to indicate how many pieces" \
		  " the seed file should be broken into and will be sent to the host addresses in this file. Should be followed by path of ssh key"



main(sys.argv[1:])
